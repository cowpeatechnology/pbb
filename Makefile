killO:
	protoc \
	--go_out=. \
	--gorm_out=. \
	--go-grpc_out=. \
	-I . \
	-I ./proto/lib \
	proto/*.proto
	protoc-go-inject-tag -input="*.pb.go"

tag:
	echo v1.3.38 > README.md
	git add *
	git commit -m "proto update"
	git tag v1.3.38
	git push origin v1.3.38

init:
	go get github.com/golang/protobuf/protoc-gen-go
	go install github.com/golang/protobuf/protoc-gen-go
	go get google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go get github.com/infobloxopen/protoc-gen-gorm
	go install github.com/infobloxopen/protoc-gen-gorm
	go get github.com/favadi/protoc-go-inject-tag
	go install github.com/favadi/protoc-go-inject-tag

# USAGE: make tag VER=v1.0.0